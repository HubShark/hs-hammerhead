<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="post", uniqueConstraints = {@ORM\UniqueConstraint(name="no_repeated_postTitle", columns={"post_title"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 */
class Post
{
    const MAXIMUM_POSTS_IN_HOMEPAGE=5;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="post_title", type="string", length=255)
     */
    private $postTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="post_description", type="text", length=2000)
     */
    private $postDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="post_content", type="text")
     */
    private $postContent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="post_creation_date", type="date")
     */
    private $postCreationDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="post_published", type="boolean")
     */
    private $postPublished;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment", mappedBy="relatedPost", cascade={"remove"})
     * @ORM\OrderBy({"commentCreationDate" = "ASC"})
     */
    private $comments;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag", mappedBy="posts")
     * @ORM\OrderBy({"tagName" = "ASC"})
     */
    private $tags; 

    /**
     * Generates the magic method
     * 
     */
    public function __toString(){
        // to show the name of the Category in the select
        return $this->postTitle;
        // to show the id of the Category in the select
        // return $this->id;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postTitle
     *
     * @param string $postTitle
     *
     * @return Post
     */
    public function setPostTitle($postTitle)
    {
        $this->postTitle = $postTitle;

        return $this;
    }

    /**
     * Get postTitle
     *
     * @return string
     */
    public function getPostTitle()
    {
        return $this->postTitle;
    }

    /**
     * Set postDescription
     *
     * @param string $postDescription
     *
     * @return Post
     */
    public function setPostDescription($postDescription)
    {
        $this->postDescription = $postDescription;

        return $this;
    }

    /**
     * Get postDescription
     *
     * @return string
     */
    public function getPostDescription()
    {
        return $this->postDescription;
    }

    /**
     * Set postContent
     *
     * @param string $postContent
     *
     * @return Post
     */
    public function setPostContent($postContent)
    {
        $this->postContent = $postContent;

        return $this;
    }

    /**
     * Get postContent
     *
     * @return string
     */
    public function getPostContent()
    {
        return $this->postContent;
    }

    /**
     * Set postCreationDate
     *
     * @param \DateTime $postCreationDate
     *
     * @return Post
     */
    public function setPostCreationDate($postCreationDate)
    {
        $this->postCreationDate = $postCreationDate;

        return $this;
    }

    /**
     * Get postCreationDate
     *
     * @return \DateTime
     */
    public function getPostCreationDate()
    {
        return $this->postCreationDate;
    }

    /**
     * Set postPublished
     *
     * @param boolean $postPublished
     *
     * @return Post
     */
    public function setPostPublished($postPublished)
    {
        $this->postPublished = $postPublished;

        return $this;
    }

    /**
     * Get postPublished
     *
     * @return bool
     */
    public function getPostPublished()
    {
        return $this->postPublished;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Comment $comment
     *
     * @return Post
     */
    public function addComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Comment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return Post
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
}
