<?php
// src/AppBundle/Form/PostType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class PostType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('postTitle', TextType::class)
            ->add('postDescription', CKEditorType::class)
            ->add('postContent', CKEditorType::class)        
            ->add('postCreationDate', DateType::class)
            ->add('postPublished', ChoiceType::class, 
                    array(
                        'choices' => array(
                                '1' => $options['yes'],
                                '0' => $options['no']
                    ),
                    'expanded' => true,
                    'multiple' => false
                )
            )
        ;
    }

#    public function setDefaultOptions(OptionsResolverInterface $resolver) {
#        $resolver->setDefaults(
#            array(
#                'data_class' => 'AppBundle\Entity\Post',
#                'yes' => null,
#                'no' => null
#            )
#        );
#    }

    public function getBlockPrefix()
    {
        return 'post';
    }
}
