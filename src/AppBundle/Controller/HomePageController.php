<?php
// src/AppBundle/Controller/HomePageController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
// Import new namespaces
use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;

class HomePageController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction() {      

        // firstResult=0 ; maxResults=Post::MAXIMUM_POSTS_IN_HOMEPAGE ; postPublished = true
        $posts=$this->getDoctrine()->getRepository('AppBundle:Post')->findAllPaginatorByPostCreationDateDESCAndPostPublished(0,Post::MAXIMUM_POSTS_IN_HOMEPAGE,true);

        return $this->render('AppBundle:HomePage:index.html.twig', array('posts' => $posts));      

    }

    /**
     * @Route("/readmore/{page}/{maxResults}", name="readmore", defaults={"page" = 1, "maxResults" = 5})
     */
    public function readMoreAction($page, $maxResults) {

        // Paginator sanitizer maxResults
        $maxResults=$this->get('app.utils')->paginatorCheckmaxResults($maxResults);
        // Calculate maxPages
        $maxPages=$this->getDoctrine()->getRepository('AppBundle:Post')->getMaxPagesPostsPublished($maxResults);
        // Paginator sanitizer page
        $page=$this->get('app.utils')->paginatorCheckpage($page,$maxPages);
        // Calculate firstResult
        $firstResult=$this->get('app.utils')->paginatorGetFirstResult($page,$maxResults);

        $posts=$this->getDoctrine()->getRepository('AppBundle:Post')->findAllPaginatorByPostCreationDateDESCAndPostPublished($firstResult,$maxResults,true);
        return $this->render(
                'AppBundle:HomePage:readmore.html.twig',
                array(
                        'posts' => $posts,
                        'page' => $page,
                        'maxPages' => $maxPages,
                        'maxResults' => $maxResults
                )
        );

    }



    /**
     * @Route("/about", name="about")
     */
    public function aboutAction() {
        return $this->render('AppBundle:Page:about.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $enquiry = new Contact();
        $form = $this->createForm(ContactType::class, $enquiry);

         $this->request = $request;
            if ($request->getMethod() == 'POST') {
            $form->bind($request);


    if ($form->isValid()) {


                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from HubShark')
                    ->setFrom('info@hubshark.com')
                    ->setTo($this->container->getParameter('app.emails.contact_email'))
                    ->setBody($this->renderView('AppBundle:Contact:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

            $this->get('session')->getFlashbag('hubshark-notice', 'Your contact enquiry was successfully sent. Thank you!');


        // Redirect - This is important to prevent users re-posting
        // the form if they refresh the page
        return $this->redirect($this->generateUrl('contact'));
    }
        }

        return $this->render('AppBundle:Contact:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }


    /**
     * @Route("/search", name="search")
     */
    public function searchAction() {
        return $this->render('AppBundle:Search:search.html.twig');
    }


    /**
     * @Route("/sidebar", name="sidebar")
     */
    public function sidebarAction() {
        return $this->render('AppBundle:Sidebar:sidebar.html.twig');
    }

}
