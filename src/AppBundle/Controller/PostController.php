<?php
// src/AppBundle/Controller/PostController.php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
// needed for hubshark
use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Comment;
use AppBundle\Form\PostType;
use AppBundle\Form\TagType;
use AppBundle\Form\CommentType;

class PostController extends Controller {

    /**
     * @Route("/admin/post/create", name="create_post")
     */ 
    public function createAction(Request $request) {

        $item=new Post();
        $item->setPostPublished(false);
        $item->setPostCreationDate(new \DateTime("now"));

        // We are creating, so the action is 1
        return $this->form($request, $item, 1);
    }

    /**
     * @Route("/admin/post/update/{item}", name="update_post")
     */ 
    public function updateAction(Request $request, Post $item) {

        // We are updating, so the action is 2
        return $this->form($request, $item, 2);
    }

    private function form(Request $request, $item, $action) {

        $form = $this->createForm(new PostType(), $item, 
                array(
                    'yes' => $this->get('translator')->trans('action.yes'),
                    'no' => $this->get('translator')->trans('action.no')
                )
        );

        $form->handleRequest($request);

        if($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($item);

            try {
                // Try to persist
                $em->flush();


            } catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {
                return $this->render('AppBundle:Form:Exception.html.twig',
                    array(
                    'message' => $this->get('translator')->trans('error.exception.uniqueconstraintviolation')));
            } catch(\Exception $e){
                return $this->render('AppBundle:Form:Exception.html.twig',
                    array(
                    'message' => $this->get('translator')->trans('error.exception.generic')));
            }

            // If we are here is because there was no exception
            // Now we send the user to "the add tags to post" menu
            return $this->redirect($this->generateUrl('menu_add_tags_to_post', array('post' => $item->getId())));
        }

        return $this->render(
                'AppBundle:Post:form.html.twig',
                array(
                        'action' => $action,
                        'form' => $form->createView()
                )
        );

    }

    /**
     * @Route("/admin/post/delete/{item}", name="delete_post")
     */ 
    public function deleteAction(Post $item) {

        $em = $this->getDoctrine()->getManager();
        $em->remove($item);

        try {
            // Try to remove
            $em->flush();

        } catch(\Exception $e){
            return $this->render('AppBundle:Form:Exception.html.twig',
                    array(
                            'message' => $this->get('translator')->trans('error.exception.generic')));
        }   

        // If we are here is because there was no exception
        return $this->redirect($this->generateUrl('manage_posts'));

    }

    /**
     * @Route("/admin/post/menuaddtags/{post}", name="menu_add_tags_to_post")
     */ 
    public function menuAddTagsAction(Post $post) {

        $allTags=$this->getDoctrine()->getRepository('AppBundle:Tag')->findTagsNotIn($post->getTags());

        return $this->render(
                'AppBundle:Post:menuaddtags.html.twig',
                array(
                        'post' => $post,
                        'allTags' => $allTags
                )
        );

    }

    /**
     * @Route("/admin/post/addtag/{post}/{tag}", name="add_tag_to_post")
     */ 
    public function addTagAction(Post $post, Tag $tag) {

        $tag->addPost($post);

        $em=$this->getDoctrine()->getManager();
        try {
            // Try to add this post to the tag
            $em->flush();

        } catch(\Exception $e){
            return $this->render('AppBundle:Form:Exception.html.twig',
                array(
                    'message' => $this->get('translator')->trans('error.exception.generic')));
        }   

        return $this->redirect($this->generateUrl('menu_add_tags_to_post', array('post' => $post->getId())));

    }

    /**
     * @Route("/admin/post/removetag/{post}/{tag}", name="remove_tag_to_article")
     */ 
    public function removeTagAction(Post $post, Tag $tag) {

        $tag->removePost($post);

        $em=$this->getDoctrine()->getManager();
        try {
            // Try to remove this post to the tag
            $em->flush();

        } catch(\Exception $e){
            return $this->render('AppBundle:Form:Exception.html.twig',
                array(
                    'message' => $this->get('translator')->trans('error.exception.generic')));
        }   

        return $this->redirect($this->generateUrl('menu_add_tags_to_post', array('post' => $post->getId())));

    }

    /**
     * @Route("/admin/post/manage", name="manage_posts")
     */ 
    public function manageAction() {

        $items=$this->getDoctrine()->getRepository('AppBundle:Post')->findAll();
        return $this->render(
                'AppBundle:Post:manage.html.twig',
                array(
                        'items' => $items
                )
        );

    }
    /**
     * @Route("/show/post/{post}/{title}", name="show_post", defaults={"title" = null})
     */
    public function showAction(Request $request, Post $post) {

        $comment=new Comment();

        $form = $this->createForm(new CommentType(), $comment);
        $form->handleRequest($request);

        if($form->isValid()) {

            $em=$this->getDoctrine()->getManager();

            $comment->setCommentCreationDate(new \DateTime("now"));
            $comment->setCommentAccepted(false);
            $comment->setRelatedPost($post);
            $post->addComment($comment);
            $em->persist($post);

            try {
                // Try to add this comment to the post
                $em->flush();

            } catch(\Exception $e){
                return $this->render('AppBundle:Form:Exception.html.twig',
                        array(
                                'message' => $this->get('translator')->trans('error.exception.generic')));
            }

            // If we are here is because there was no exception
            return $this->redirect($this->generateUrl('show_post', array('post' => $post->getId())));

        }

        return $this->render('AppBundle:Post:show.html.twig',
                array(
                        'post' => $post,
                        'form' => $form->createView()
                )
        );

    }

}
